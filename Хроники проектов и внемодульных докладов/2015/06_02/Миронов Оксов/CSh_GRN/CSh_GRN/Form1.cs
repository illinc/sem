﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CSh_GRN{
    public partial class Form1 : Form{
        int R_;
        public Form1(int Rand_){
            //Init components:
            InitializeComponent();
            
            R_ = 2345;// Start random const numbeR
            textBox2.Text = System.Convert.ToString(R_);

            //Init ABC for parsing:
            useless = "zxcvbnm1234567890";
            letters = new string[2];
            letters[0] = "qwertyuiopasdfghjkl";
            letters[1] = "lkjhgfdsapoiuytrewq";
        }
        //--------------------------------------------------------
        string useless;
        string[] letters;

        // Test function:
        int check(int current, int offset, bool forward){
            // Searching words:
            int found = -1;
            found = useless.IndexOf((char)current );
            
            // Break:
            if( found !=-1 ) return -1;

            // Continue searching:
            found = letters[forward ? 0 : 1].IndexOf( (char)current );
            if (found - offset > 9) return -1;

            return found - offset;
        }
        //--------------------------------------------------------
        private void button1_Click(object sender, EventArgs e){
            // Test input string:
            if (textBox1.Text != "") {
                // Tmp varriables
                int random_number = R_;// Set random num
                string answer;
                int current;
                int sum;
                int change=0;
                bool forward=true;
                int offset;
                bool up_down;

                // Alloc memory for result: 
                StringBuilder result = new StringBuilder("");

                // Answer string
                answer = System.Convert.ToString(textBox1.Text);
                // Computate sum var
                sum=random_number/1000 + (random_number % 1000)/100 + ((random_number % 1000) % 100) /10 + random_number % 10;

                // Test on parity:
                if (sum % 2 ==0) forward = true;
                else forward = false;

                // Debug information:
                System.Console.Out.Write("\nsumma cifr = "+sum.ToString()+" => forward = "+forward.ToString() );
                offset = random_number/1000;
                System.Console.Out.Write("\noffset ="+offset.ToString());
                // Debug information:
                if (!forward) System.Console.Out.Write("\nBackwards!");

                //For all leters in input word:
                for (int i = 0; i < answer.Length; i++ ){
                    change = check ( answer[i], offset, forward );
                    System.Console.Out.Write("\nChange [" + i.ToString() + "] from _" + answer[i].ToString() + "_ = " + change.ToString() );//printf("\nChange [%d] from _%c_ = %d", i, answer[i], change);
                   
                    // Was changed?
                    if ( change != -1){
                        result.Append( change.ToString() );
                        System.Console.Out.Write("\ntemp result = " + result.ToString() );// Debug help
                    }
                }

                // Get number from input string:
                int.TryParse(result.ToString(), out R_);

                // Test on num len
                if (result.Length > 4){
                    Random rand = new Random();
                    R_ = rand.Next(1000, 10001);
                }

                // Set text to text box:
                textBox1.Text = "";

            }else{
                //Pure random from generator:
                Random rand = new Random();
                R_ = rand.Next(1000, 10001);
            }
            //Text output to screen:
            textBox2.Text = System.Convert.ToString(R_);
        }
    }
}
