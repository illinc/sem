﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CSh_GRN
{
    public partial class Form1 : Form
    {
        int R_;
        public Form1(int Rand_)
        {
            InitializeComponent();
            R_ = Rand_;
            textBox2.Text = System.Convert.ToString(R_);
        }

        private void button1_Click(object sender, EventArgs e){
            int result;
            if (textBox1.Text != "") {
                int a, b, c, d;

                a = R_/ 1000;
                b = R_ / 100 - 10 * a;
                c = R_ / 10 - 100 * a - 10 * b;
                d = R_ % 10;
            
                string str = System.Convert.ToString(textBox1.Text);

                int min = 10;
                if (a < min && a!=0) min = a;
                if (b < min && b != 0) min = b;
                if (c < min && c != 0) min = c;
                if (d < min && d != 0) min = d;

                int sum = R_ / min;
                char[] ch = new char[str.Length];
                ch = str.ToCharArray();
                for(int i=0; i<str.Length; i+=2){
                    sum += (int)ch[i];
                }

                result = sum;
                R_ = sum;
                textBox1.Text = "";
            }else{
                Random rand = new Random();
                R_ = rand.Next(1, 10001);
                result = R_;
            }

            textBox2.Text = System.Convert.ToString(result);

        }
    }
}
